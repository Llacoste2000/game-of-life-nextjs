import {
  Box,
  Button,
  Flex,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from "@chakra-ui/react";
import Head from "next/head";
import React, { useCallback, useEffect } from "react";
import { Cell } from "../components/Cell";
import { useGame } from "../hooks/useGame";

const FullscreenPage = () => {
  const pixelRatio = 1.5 * 5;
  const {
    setDimentions,
    dimentions: { columns },
    grid,
    handleRun,
    handleStop,
    handleClear,
    handleRandomize,
  } = useGame();

  const setupFullscreenGrid = useCallback(() => {
    const width = window.innerWidth;
    const height = window.innerHeight;

    const rows = Math.floor(height / pixelRatio);
    const columns = Math.floor(width / pixelRatio);

    setDimentions({ rows, columns });
    handleRandomize();
  }, [handleRandomize, pixelRatio, setDimentions]);

  useEffect(() => {
    setupFullscreenGrid();
  }, [setupFullscreenGrid]);

  const handleUpdate = useCallback(() => {
    setupFullscreenGrid();
  }, [setupFullscreenGrid]);

  if (isNaN(pixelRatio)) {
    return <Box>Invalid pixelRatio, must be a number</Box>;
  }

  return (
    <Box h="100vh" w="100vw" position="relative" bg="#111">
      <Actions
        handleRun={handleRun}
        handleStop={handleStop}
        handleClear={handleClear}
        handleRandomize={handleRandomize}
        handleUpdate={handleUpdate}
      />
      <Flex w={`${columns * pixelRatio}px`} flexWrap="wrap">
        {grid.map((row, i) =>
          row.map((cell, j) => (
            <Cell
              alive={cell}
              key={`${i}-${j}`}
              h={`${pixelRatio}px`}
              w={`${pixelRatio}px`}
            />
          )),
        )}
      </Flex>
    </Box>
  );
};

export default FullscreenPage;

const Actions = React.memo(function Actions({
  handleRun,
  handleStop,
  handleClear,
  handleRandomize,
  handleUpdate,
}: {
  handleRun: () => void;
  handleStop: () => void;
  handleClear: () => void;
  handleRandomize: () => void;
  handleUpdate: () => void;
}) {
  return (
    <>
      <Head>
        <title>GOL NextJs</title>
      </Head>
      <Box position="absolute" top={4} left={4}>
        <Menu>
          <MenuButton as={Button}>Actions</MenuButton>
          <MenuList>
            <MenuItem onClick={handleRun}>Run</MenuItem>
            <MenuItem onClick={handleStop}>Stop</MenuItem>
            <MenuItem onClick={handleClear}>Clear</MenuItem>
            <MenuItem onClick={handleRandomize}>Randomize</MenuItem>
            <MenuItem onClick={handleUpdate}>Update</MenuItem>
          </MenuList>
        </Menu>
      </Box>
    </>
  );
});
