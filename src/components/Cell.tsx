import { Box, BoxProps } from "@chakra-ui/react";
import React from "react";

type CellProps = {
  alive: number;
} & BoxProps;

export const Cell = React.memo(function Cell({ alive, ...rest }: CellProps) {
  return <Box bg={alive === 1 ? "#F4F4F4" : "#111"} {...rest} />;
});
