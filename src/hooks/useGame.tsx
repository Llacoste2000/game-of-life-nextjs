import React, { useCallback, useMemo, useRef, useState } from "react";

const DIRECTIONS = [
  [-1, -1],
  [-1, 0],
  [-1, 1],
  [0, -1],
  [0, 1],
  [1, -1],
  [1, 0],
  [1, 1],
];

export const useGame = () => {
  const [dimentions, setDimentions] = useState<{
    rows: number;
    columns: number;
  }>({ rows: 0, columns: 0 });
  const [grid, setGrid] = useState<number[][]>([]);
  const running = useRef(false);

  const tick = useCallback(() => {
    setGrid((grid) => {
      const newGrid = JSON.parse(JSON.stringify(grid));

      for (let i = 0; i < dimentions.rows; i++) {
        for (let j = 0; j < dimentions.columns; j++) {
          let neighbors = 0;
          for (let k = 0; k < DIRECTIONS.length; k++) {
            const [x, y] = DIRECTIONS[k];
            const newI = i + x;
            const newJ = j + y;

            if (
              newI >= 0 &&
              newI < dimentions.rows &&
              newJ >= 0 &&
              newJ < dimentions.columns
            ) {
              neighbors += grid[newI][newJ];
            }
          }

          if (neighbors < 2 || neighbors > 3) {
            newGrid[i][j] = 0;
          } else if (grid[i][j] === 0 && neighbors === 3) {
            newGrid[i][j] = 1;
          }
        }
      }

      return newGrid;
    });
  }, [dimentions.columns, dimentions.rows]);

  const run = useCallback(() => {
    if (!running.current) {
      return;
    }

    tick();
    setTimeout(run, 50);
  }, [tick]);

  const handleRandomize = useCallback(() => {
    setGrid(
      Array.from(
        {
          length: dimentions.rows,
        },
        () =>
          Array.from(
            {
              length: dimentions.columns,
            },
            () => Math.floor(Math.random() * 2),
          ),
      ),
    );
  }, [dimentions.columns, dimentions.rows]);

  const handleClear = useCallback(() => {
    setGrid(
      Array.from(
        {
          length: dimentions.rows,
        },
        () =>
          Array.from(
            {
              length: dimentions.columns,
            },
            () => 0,
          ),
      ),
    );
  }, [dimentions.columns, dimentions.rows]);

  const handleStop = useCallback(() => {
    running.current = false;
  }, []);

  const handleRun = useCallback(() => {
    running.current = true;
    run();
  }, [run]);

  return useMemo(
    () => ({
      dimentions,
      setDimentions,
      grid,
      setGrid,
      handleRun,
      handleStop,
      handleClear,
      handleRandomize,
      run,
    }),
    [
      dimentions,
      grid,
      handleClear,
      handleRandomize,
      handleRun,
      handleStop,
      run,
    ],
  );
};
