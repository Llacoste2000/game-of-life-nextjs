FROM node:16.18-alpine3.17 as builder

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
RUN npm ci

COPY . ./
RUN npm run build && npm run export

FROM nginx:1.23.3-alpine

ADD ./nginx.conf /etc/nginx/conf.d/default.template
COPY --from=builder /app/out /usr/share/nginx/html

CMD sh -c "envsubst \"`env | awk -F = '{printf \" \\\\$%s\", $1}'`\" < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"